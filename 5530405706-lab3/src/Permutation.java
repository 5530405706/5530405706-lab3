
public class Permutation {

	private static long n1;
	private static long n2;

	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Usage:Permutation <n> <k>");
			System.exit(1);
		}

		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		long result = permute(n, k);
		System.out.println(n + "!/(" + n + "-" + k + ")! = " + result);

	}

	static long permute(int n, int k) {
		long ans = 0;
		n1 = 1;
		n2 = n - k;
		int n3 = 1;
		for (int i = 1; i <= n; i++) {
			n1 = n1 * i;

		}
		for (int j = 1; j <= n2; j++) {
			n3 = n3 * j;

		}
		System.out.println(n + "! = " + n1);
		System.out.println("(" + n + "-" + k + ")! = " + n3);
		ans = n1 / n3;

		return ans;


	}

}
